<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_payment', function (Blueprint $table) {
            $table->id();
            $table->string('invoice');
            $table->decimal('amount');
            $table->string('type');
            $table->string('reference');
            $table->string('status');
            $table->string('verified_by');
            $table->string('verified_at');
          $table->unsignedInteger('contract_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contract_payment');
    }
};
