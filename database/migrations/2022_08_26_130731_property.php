<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('status');
            $table->string('city_id');
            $table->string('location_long');
            $table->string('location_lat');
            $table->string('admin_approvel');
            $table->decimal('daily_price');
            $table->decimal('monthly_price');
            $table->decimal('annual_price');
            $table->decimal('gross_price');
            $table->string('electricity_included');
            $table->string('water_included');
            $table->string('wifi_included');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('propertyGroup_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property');
    }
};
