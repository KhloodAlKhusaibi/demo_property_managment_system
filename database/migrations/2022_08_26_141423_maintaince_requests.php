<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintaince_requests', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->decimal('price');
            $table->string('message');
            $table->string('status');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('property_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maintaince_requests');
    }
};
